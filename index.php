<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://fonts.googleapis.com/css?family=Share+Tech+Mono" rel="stylesheet" type="text/css" />
    <title>e-Monroy - Desarrollador Web y Front - end</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/estilos.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body data-spy="scroll" data-target=".navbar-example">
    
	<div class="container contenedor">
		<div class="col-sm-4 ">

		<div id="cristihan">
			<div class="redes-sociales" >
				<ul class="list-inline">
					<li>
						<strong style="color: black">Sigueme en:</strong>
					</li>
					<li>
						<a href="http://twitter.com/@cmonroy_co">
							<img src="img/twitter.png" alt="Twitter - @cmonroy_co" target="_back">
						</a>
					</li>
					<li>
						<img src="img/google-plus.png" alt="Google Plus - Cristihan Monroy">
					</li>
					<li>
						<img src="img/linkedin.png" alt="Linkedin - Cristihan Monroy ">
					</li>
				</ul>
			</div>
			<div class="pefil">
				<div class="text-center">
					<img class="img-responsive img-circle foto" src="img/Cristihan_Monroy.jpg" alt="Foto Perfil - Cristihan Andres Monroy ">
					<h3>Cristihan Andres Monroy<br>
						<small>Desarrollador Front End - Back End</small>
					</h3>
					

				</div>
			</div>	
			<div class="datos-perfil">
				<div class="btn-group btn-group-justified">
				  <div class="btn-group">
				    <button type="button" class="btn btn-default">Mi Tarjeta</button>
				  </div>
				  <div class="btn-group">
				    <button type="button" class="btn btn-default">Contratame</button>
				  </div>
				  <div class="btn-group">
				    <button type="button" class="btn btn-default">Información</button>
				  </div>
				</div>
				<div class="footer-pefil">
					<p class="text-center">
						<small>e-Monroy.com - 2014</small>
					</p>
				</div>
				
			</div>			
		</div>


		</div>
		<div class="col-sm-8">

			<div class="menu-top " >
				<ul class="list-inline">
					<li>
						<a href="#Perfil">Perfil</a>
					</li>
					<li>
						<a href="#Portafolio">Portafolio</a>
					</li>
					<li>
						<a href="#Contacto">Contacto</a>
					</li>
				</ul>
			</div>


			<div class="contenedor-info" >
				<div class="item" id="Perfil">
					<h3>Perfil</h3>
					<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste incidunt, ratione maxime possimus, optio fuga molestias obcaecati! Facilis, ut repudiandae eaque soluta ipsum, mollitia neque veritatis, recusandae hic pariatur reprehenderit.</div>
					<div>Iure officiis animi maxime enim dolorum placeat debitis, fuga sapiente tenetur optio aliquam, possimus incidunt repellendus eum, similique facilis beatae quis perspiciatis ipsa dolores voluptatum, omnis ea molestiae quo. Cum!</div>
					<div>Laudantium illum, iste ipsam fugit corporis aperiam quod repellendus, minima minus consequuntur necessitatibus fuga alias, quaerat mollitia labore sit veniam fugiat nisi impedit pariatur saepe ea iusto beatae! Deleniti, autem!</div>
					<div>Adipisci officiis voluptatibus possimus illum aspernatur natus nostrum voluptas necessitatibus corrupti, voluptatem incidunt maiores facere iure laboriosam, nam aut delectus architecto similique, ea commodi repudiandae minima voluptate quia perspiciatis. Tenetur.</div>
				</div>

				<div class="item" id="Portafolio">
					<h3>Portafolio</h3>
					<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste incidunt, ratione maxime possimus, optio fuga molestias obcaecati! Facilis, ut repudiandae eaque soluta ipsum, mollitia neque veritatis, recusandae hic pariatur reprehenderit.</div>
					<div>Iure officiis animi maxime enim dolorum placeat debitis, fuga sapiente tenetur optio aliquam, possimus incidunt repellendus eum, similique facilis beatae quis perspiciatis ipsa dolores voluptatum, omnis ea molestiae quo. Cum!</div>
					<div>Laudantium illum, iste ipsam fugit corporis aperiam quod repellendus, minima minus consequuntur necessitatibus fuga alias, quaerat mollitia labore sit veniam fugiat nisi impedit pariatur saepe ea iusto beatae! Deleniti, autem!</div>
					<div>Adipisci officiis voluptatibus possimus illum aspernatur natus nostrum voluptas necessitatibus corrupti, voluptatem incidunt maiores facere iure laboriosam, nam aut delectus architecto similique, ea commodi repudiandae minima voluptate quia perspiciatis. Tenetur.</div>
				</div>

				<div class="item" id="Contacto">
					<h3>Contacto</h3>
					<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste incidunt, ratione maxime possimus, optio fuga molestias obcaecati! Facilis, ut repudiandae eaque soluta ipsum, mollitia neque veritatis, recusandae hic pariatur reprehenderit.</div>
					<div>Iure officiis animi maxime enim dolorum placeat debitis, fuga sapiente tenetur optio aliquam, possimus incidunt repellendus eum, similique facilis beatae quis perspiciatis ipsa dolores voluptatum, omnis ea molestiae quo. Cum!</div>
					<div>Laudantium illum, iste ipsam fugit corporis aperiam quod repellendus, minima minus consequuntur necessitatibus fuga alias, quaerat mollitia labore sit veniam fugiat nisi impedit pariatur saepe ea iusto beatae! Deleniti, autem!</div>
					<div>Adipisci officiis voluptatibus possimus illum aspernatur natus nostrum voluptas necessitatibus corrupti, voluptatem incidunt maiores facere iure laboriosam, nam aut delectus architecto similique, ea commodi repudiandae minima voluptate quia perspiciatis. Tenetur.</div>
				</div>


			</div>


		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script>
    $('.menu-top').find('ul').affix({
    	offset: {
    		top: 0
    	}
    })
    </script>


  </body>
</html>